import { AbstractControl } from '@angular/forms';

export function dateFormatValidator(control: AbstractControl): {[key: string]: any} | null {
  if (!control.value) {
    return null;
  }

  const dateRegExp = /^(0[1-9]|1\d|2\d|3[01])\.(0[1-9]|1[0-2])\.(19|20)\d{2}$/;
  const incorrectDate = !dateRegExp.test(control.value);
  return incorrectDate ? {'dateFormat': {value: control.value}} : null;
}
