import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { AddTodoComponent } from './todo-list/add-todo/add-todo.component';
import { EditTodoComponent } from './todo-list/edit-todo/edit-todo.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'add-todo', component: AddTodoComponent},
  {path: 'edit-todo/:id', component: EditTodoComponent},
  {path: '', component: TodoListComponent, pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
