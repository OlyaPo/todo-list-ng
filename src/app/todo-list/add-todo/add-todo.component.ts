import { Component } from '@angular/core';
import { ToDoService } from '../todo-service';
import { ToDo } from '../to-do.interface';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.scss']
})
export class AddTodoComponent {
  buttonText = 'Add';

  constructor(
    private toDoService: ToDoService,
  ) { }

  addToDo(newToDo: ToDo) {
    this.toDoService.addToDo(newToDo);
  }
}
