export interface ToDo {
  id?: number;
  task: string;
  description?: string;
  completed?: boolean;
  isImportant: boolean;
  board: string;
}
