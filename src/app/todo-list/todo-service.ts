import { Injectable } from '@angular/core';
import { ToDo } from './to-do.interface';
import { Subject } from 'rxjs';

@Injectable()
export class ToDoService {
  private todos: ToDo[] = [
    {
      id: 1,
      task: 'prepare presentation',
      completed: false,
      isImportant: false,
      board: 'work',
    },
    {
      id: 2,
      task: 'add examples',
      completed: false,
      isImportant: true,
      board: 'work',
    },
  ];

  private boards = ['work', 'home'];

  private activeBoard = this.boards[0];

  activeBoardSubject = new Subject();
  toDosSubject = new Subject();

  getToDos(): ToDo[] {
    return this.todos;
  }

  getTodo(id: number): ToDo {
    return this.todos.find((todo) => todo.id === id);
  }

  addToDo(todo: ToDo) {
    todo.id = this.todos.length + 1;
    todo.completed = false;
    this.todos.push(todo);
    this.toDosSubject.next(this.getFilteredByBoardToDos());
  }

  editToDo(id, newTodo: ToDo) {
    const index = this.todos.findIndex((todo) => todo.id === id);
    this.todos[index] = newTodo;
  }

  deleteToDo(id: number) {
    const index = this.todos.findIndex((todo) => todo.id === id);
    this.todos.splice(index, 1);
    this.toDosSubject.next(this.getFilteredByBoardToDos());
  }

  toggleToDoCompleted(id: number) {
    const index = this.todos.findIndex((todo) => todo.id === id);
    this.todos[index].completed = !this.todos[index].completed;
    this.toDosSubject.next(this.getFilteredByBoardToDos());
  }

  getBoards(): string[] {
    return this.boards;
  }

  getActiveBoard(): string {
    return this.activeBoard;
  }

  setActiveBoard(board) {
    this.activeBoard = board;
    this.activeBoardSubject.next(this.activeBoard);
  }

  getFilteredByBoardToDos(): ToDo[] {
    return this.todos.filter((todo) => todo.board === this.activeBoard);
  }
}
