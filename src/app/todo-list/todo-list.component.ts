import { Component, OnInit } from '@angular/core';

import { ToDo } from './to-do.interface';
import { ToDoService } from './todo-service';
import { Router } from '@angular/router';

import { faPlus } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  todos: ToDo[] = [];
  faPlus = faPlus;

  constructor(
    private toDoService: ToDoService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getToDos();

    this.toDoService.activeBoardSubject.subscribe(() => {
      this.getToDos();
    });

    this.toDoService.toDosSubject.subscribe((todos: ToDo[]) => {
      this.todos = todos;
    });
    // why you don't need to unsubscribe from Subject
    // https://blog.angularindepth.com/rxjs-closed-subjects-1b6f76c1b63c
  }

  deleteToDo(id: number) {
    this.toDoService.deleteToDo(id);
  }

  completeToDo(id: number) {
    this.toDoService.toggleToDoCompleted(id);
  }

  goToAddToDoPage() {
    this.router.navigate(['/add-todo']);
  }

  getToDos() {
    this.todos = this.toDoService.getFilteredByBoardToDos();
  }
}
