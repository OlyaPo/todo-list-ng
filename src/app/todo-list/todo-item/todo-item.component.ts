import { Component, Input, Output, EventEmitter} from '@angular/core';

import { faExclamationCircle, faTrashAlt, faCheck, faPencilAlt } from '@fortawesome/free-solid-svg-icons';

import {ToDo} from '../to-do.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent {
  @Input() todo: ToDo;
  @Output() deleteToDo = new EventEmitter();
  @Output() completeToDo = new EventEmitter();

  faExclamationCircle = faExclamationCircle;
  faTrashAlt = faTrashAlt;
  faCheck = faCheck;
  faPencilAlt = faPencilAlt;

  constructor(
    private router: Router,
  ) {}

  delete() {
    this.deleteToDo.emit();
  }

  complete() {
    this.completeToDo.emit();
  }

  goToEditToDo() {
    this.router.navigate([`/edit-todo/${this.todo.id}`]);
  }
}
