import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToDoService } from '../todo-service';
import { ToDo } from '../to-do.interface';

@Component({
  selector: 'app-edit-todo',
  templateUrl: './edit-todo.component.html',
  styleUrls: ['./edit-todo.component.scss']
})
export class EditTodoComponent implements OnInit {
  todo: ToDo;
  buttonText = 'Edit';

  constructor(
    private route: ActivatedRoute,
    private todoService: ToDoService,
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.todo = this.todoService.getTodo(Number(id));
  }

  editTodo(newToDo: ToDo) {
    this.todoService.editToDo(this.todo.id, newToDo);
  }
}
