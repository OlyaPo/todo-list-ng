import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ToDoService } from '../todo-service';
import { ToDo } from '../to-do.interface';
import { Router } from '@angular/router';
import { dateFormatValidator } from 'src/app/shared/date-format-validator';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.scss']
})
export class TodoFormComponent implements OnInit {
  @Input() todo: ToDo;
  @Input() buttonText = '';
  @Output() submitHandler = new EventEmitter();

  boards: string[] = [];
  toDoForm: FormGroup;

  dateRegExp = /^(0[1-9]|1\d|2\d|3[01])\.(0[1-9]|1[0-2])\.(19|20)\d{2}$/;

  constructor(
    private toDoService: ToDoService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.boards = this.toDoService.getBoards();

    this.toDoForm = new FormGroup({
      task: new FormControl(this.todo ? this.todo.task : '', [
        Validators.minLength(2),
        Validators.required,
      ]),
      description: new FormControl(this.todo ? this.todo.description : ''),
      isImportant: new FormControl(this.todo ? this.todo.isImportant : false),
      board: new FormControl(this.todo ? this.todo.board : this.boards[0]),
      // date: new FormControl('', Validators.pattern(this.dateRegExp)),
      date: new FormControl('', dateFormatValidator),
    });

    // subscribe for value changes
    // this.task.valueChanges.subscribe((newValue) => {
    //   console.log(newValue);
    // });
  }

  onSubmit() {
    console.log(this.toDoForm);
    this.submitHandler.emit(this.toDoForm.value);
    this.router.navigate(['/']);
  }

  get task() {
    return this.toDoForm.get('task');
  }

  get date() {
    return this.toDoForm.get('date');
  }
 }
