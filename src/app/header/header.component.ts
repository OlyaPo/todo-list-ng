import { Component, OnInit } from '@angular/core';
import { ToDoService } from '../todo-list/todo-service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  boards: string[] = [];
  selectedBoard = '';

  constructor(
    private toDoService: ToDoService
  ) { }

  ngOnInit() {
    this.boards = this.toDoService.getBoards();
    this.selectedBoard = this.toDoService.getActiveBoard();
  }

  changeBoard() {
    this.toDoService.setActiveBoard(this.selectedBoard);
  }
}
